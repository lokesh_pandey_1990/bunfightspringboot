package com.bunfight.controller.authentication;

import com.bunfight.model.authentication.BfsAuthentication;
import com.bunfight.services.authenticate.BfsAuthService;
import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDateTime;
import java.util.UUID;

/*
Author - Lokesh Pandey
*/

@RunWith(SpringRunner.class)
@SpringBootTest
public class BfsAuthenticationControllerTest {

    @Autowired
    BfsAuthenticationController bfsAuthenticationController;
    @Autowired
    BfsAuthService bfsAuthService;

    @Test
    public void testCreateUser() {
        BfsAuthentication user = new BfsAuthentication(UUID.fromString("ff0177fe-494f-43d1-afbc-38d11d7fe90b"),
                "xyz", "786lokeshpandey@gmail.com", 1236542890L,
                "password", LocalDateTime.parse("2020-10-22T10:45:15"), false,
                false, false);
        bfsAuthenticationController.registerUser(user);
        Iterable<BfsAuthentication> users = bfsAuthService.findAll();
        Assertions.assertThat(users).extracting(BfsAuthentication::getUserEmailId)
                .contains("786lokeshpandey@gmail.com");
    }
}
