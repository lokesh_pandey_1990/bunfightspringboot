package com.bunfight.controller.bfsservices.hallService;

import com.bunfight.model.bfsservices.hallService.HallServiceBookedModel;
import com.bunfight.services.bfsservices.hallService.HallServiceBooked;
import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDateTime;
import java.util.UUID;

/*
Author - Lokesh Pandey
*/

@RunWith(SpringRunner.class)
@SpringBootTest
public class HallServiceBookingControllerTests {

    @Autowired
    ServiceBookedController serviceBookedController;
    @Autowired
    HallServiceBooked services;

    @Test
    public void bookService() {
        HallServiceBookedModel bookedModel = new HallServiceBookedModel(UUID.fromString("ff0177fe-494f-43d1-afbc-38d11d7fe90b"),
                "ff0177fe-494f-43d1-afbc-38d11d7fe90b", "ff0177fe-494f-43d1-afbc-38d11d7fe90b",
                LocalDateTime.parse("2020-10-22T10:45:15"), "Marriage", 1,LocalDateTime.parse("2020-10-22T10:45:15"),
                LocalDateTime.parse("2020-10-22T10:45:15"), "Decoration,Dining",
                10000.0, true, 1000.00, 1000.00);
        serviceBookedController.bookService(bookedModel);
        Iterable<HallServiceBookedModel> servicesBooked = services.findAll();
        Assertions.assertThat(servicesBooked).extracting(HallServiceBookedModel::getEndUserUuid)
                .contains("ff0177fe-494f-43d1-afbc-38d11d7fe90b");
    }
}
