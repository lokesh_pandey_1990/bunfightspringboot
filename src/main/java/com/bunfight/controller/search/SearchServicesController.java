package com.bunfight.controller.search;

import com.bunfight.common.models.SearchControllerModel;
import com.bunfight.model.bfsservices.hallService.HallServiceInformationTable;
import com.bunfight.services.bfsservices.hallService.HallServiceInformation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;
import java.util.List;

/*
Author - Lokesh Pandey
*/

@RestController
public class SearchServicesController {

    @Autowired
    HallServiceInformation hallServiceInformation;

    @GetMapping("/bfs/v1/search/service")
    public List<HallServiceInformationTable> searchServices(@RequestBody SearchControllerModel searchModel) {
        final String cityName = searchModel.getCityName();
        final String serviceNeededFor = searchModel.getServiceNeededFor();
        final String fromDate = searchModel.getFromDate();
        final String toDate = searchModel.getToDate();
        List<HallServiceInformationTable> searchResults;
        if(cityName != null && !cityName.isEmpty() && serviceNeededFor != null && !serviceNeededFor.isEmpty() &&
                fromDate != null && toDate != null) {
             searchResults = hallServiceInformation.findByHallServiceCity(cityName,
                    serviceNeededFor, fromDate, toDate);
            return searchResults;
        } else if(searchModel.getLatitude() != 0.0d && searchModel.getLongitude() != 0.0d &&
                serviceNeededFor != null && !serviceNeededFor.isEmpty() && fromDate != null &&
                serviceNeededFor != null && toDate != null) {
            searchResults = hallServiceInformation.findByHallServiceLatitudeAndHallServiceLongitude(
                    searchModel.getLatitude(), searchModel.getLongitude(), serviceNeededFor, fromDate, toDate
            );
            return searchResults;
        }
        return null;
    }
}
