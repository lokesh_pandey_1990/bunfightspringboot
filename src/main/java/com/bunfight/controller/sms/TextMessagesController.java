package com.bunfight.controller.sms;

import com.bunfight.common.sns.BunfightSimpleNotificationService;
import com.bunfight.model.sms.SMSModel;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/*
Author - Lokesh Pandey
*/

@RestController
public class TextMessagesController {

    @PostMapping("/bfs/v1/send/message")
    public void sendTextMessages(@RequestBody SMSModel smsModel) {
        BunfightSimpleNotificationService sms = new BunfightSimpleNotificationService();
        sms.sendSMSMessage(smsModel.getPhoneNumber(), smsModel.getTextMessage());
    }
}
