package com.bunfight.controller.authentication;

import com.bunfight.common.models.UserCredentials;
import com.bunfight.common.apierrors.EntityNotFoundException;
import com.bunfight.common.apierrors.AlreadyExistsException;
import com.bunfight.common.generate.OnRegistrationGeneratePassword;
import com.bunfight.common.sendemails.EmailAttributes;
import com.bunfight.common.sendemails.SendingEmails;
import com.bunfight.common.sessionandtokens.UserSessionComponent;
import com.bunfight.model.authentication.BfsAuthentication;
import com.bunfight.services.authenticate.BfsAuthService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import static org.springframework.security.web.context.HttpSessionSecurityContextRepository.SPRING_SECURITY_CONTEXT_KEY;

/*
Author - Lokesh Pandey
*/

@RestController
public class BfsAuthenticationController {
    private static final Logger bfsAuthLogger = LoggerFactory.getLogger(BfsAuthenticationController.class);

    @Autowired
    BfsAuthService bfsAuthService;
    @Autowired
    AuthenticationManager authenticationManager;

    @PostMapping("/bfs/v1/register/user")
    BfsAuthentication registerUser(@RequestBody BfsAuthentication bfsAuthentication) throws AlreadyExistsException {
        if(checkIfUserExists(bfsAuthentication)) {
            throw new AlreadyExistsException("You have already registered. Try login !");
        }
        OnRegistrationGeneratePassword reg = new OnRegistrationGeneratePassword();
        Map<String, String> passwordList = reg.generateRandomAndHashedPassword();
        String password = passwordList.get("hashedPassword");
        bfsAuthentication.setUserPassword(password);
        BfsAuthentication registeredUser = bfsAuthService.save(bfsAuthentication);
        sendWelcomeEmailToUsers(registeredUser, passwordList);
        return registeredUser;
    }

    private boolean checkIfUserExists(BfsAuthentication bfsAuthentication) {
        BfsAuthentication bfsAuthentication1 = bfsAuthService.findByUserEmailId(bfsAuthentication.getUserEmailId());
        if(bfsAuthentication1 != null && bfsAuthentication1.getUserEmailId() != null) {
            return true;
        }
        BfsAuthentication bfsAuthentication2 = bfsAuthService.findByUserPhoneNumber(bfsAuthentication.getUserPhoneNumber());
        if(bfsAuthentication2 != null && bfsAuthentication2.getUserPhoneNumber() != null)
            return true;
        return false;
    }

    @GetMapping("/bfs/v1/login/user")
    public UserSessionComponent loginUser(HttpServletRequest request,
                                          @RequestBody UserCredentials userCredentials) throws EntityNotFoundException {
        String requestedSessionId = request.getRequestedSessionId();
        SecurityContext securityContext;
        if(requestedSessionId != null) {
            if(request.getSession().getAttribute(SPRING_SECURITY_CONTEXT_KEY) != null) {
                securityContext = (SecurityContext) request.getSession().getAttribute(SPRING_SECURITY_CONTEXT_KEY);
            } else {
                securityContext = authenticateAndSetSessionSecurityContext(request, userCredentials);
            }
        } else {
            securityContext = authenticateAndSetSessionSecurityContext(request, userCredentials);
        }
        UserSessionComponent user = (UserSessionComponent) securityContext.getAuthentication().getPrincipal();
        return user;
    }

    private SecurityContext authenticateAndSetSessionSecurityContext(HttpServletRequest request,
                                                                          UserCredentials userCredentials) {
        String userLoginWay = userCredentials.getUserLoginWay();
        String password = userCredentials.getPassword();
        UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken =
                new UsernamePasswordAuthenticationToken(userLoginWay, password);
        Authentication authentication = authenticationManager.authenticate(usernamePasswordAuthenticationToken);
        SecurityContext securityContext = SecurityContextHolder.getContext();
        UserSessionComponent user = (UserSessionComponent) securityContext.getAuthentication().getPrincipal();
        if(!user.isEmailIdVerified()) {
            bfsAuthService.updateEmailVerified(UUID.fromString(user.getUserUuid()));
            user.setEmailIdVerified(true);
        }
        securityContext.setAuthentication(authentication);
        HttpSession session = request.getSession(true);
        session.setAttribute(SPRING_SECURITY_CONTEXT_KEY, securityContext);
        return securityContext;
    }

    private void sendWelcomeEmailToUsers(BfsAuthentication registeredUser, Map<String, String> passwordList) {
        SendingEmails sendingEmails = new SendingEmails();
        EmailAttributes emailAttributes = new EmailAttributes();
        List<String> toEmailAddress = new ArrayList<>();
        String subject = "Welcome to the Bunfight family";
        String body = "Welcome to bunfight. These are your credentials for first time login.\n\n" +
                "emailId : " + registeredUser.getUserEmailId() + " \n" +
                "password : " + passwordList.get("readablePassword") + "\n\n" +
                "Please don't forget to change you password after first time login."+
                "\n\nWith Love\nBunfight";
        emailAttributes.setSubject(subject);
        emailAttributes.setBody(body);
        toEmailAddress.add(registeredUser.getUserEmailId());
        emailAttributes.setToEmailAddresses(toEmailAddress);
        sendingEmails.sendEmails(emailAttributes);
    }
}
