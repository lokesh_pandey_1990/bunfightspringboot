package com.bunfight.controller.bfsservices.hallService;

import com.bunfight.common.apierrors.IllegalAuthorityException;
import com.bunfight.common.sessionandtokens.UserSessionComponent;
import com.bunfight.common.sessiondetails.GetDetailsFromSessionId;
import com.bunfight.model.bfsservices.hallService.HallServicePaymentRecords;
import com.bunfight.services.bfsservices.hallService.HallServicePayments;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.Collection;

/*
Author - Lokesh Pandey
*/

@RestController
public class HallServicePaymentRecordsController {

    @Autowired
    HallServicePayments payments;

    /*TODO
     *  Booking must be happen after the payment. This is just to test the booking API.
    */

    @PutMapping("/bfs/v1/payment/gateway")
    public HallServicePaymentRecords paymentMethods(HttpServletRequest request,
                                                    @RequestBody HallServicePaymentRecords paymentRecords) {
        /*TODO
        *  Should call book now to finish the booking first and then update the payment record table
        */
        GetDetailsFromSessionId getDetailsFromSessionId = new GetDetailsFromSessionId();
        UserSessionComponent user = getDetailsFromSessionId.getSessionDetails(request);
        Collection<GrantedAuthority> authority = user.getAuthorities();
        authority.forEach(auth -> {
            if(!auth.getAuthority().equals("ROLE_END_USER")) {
                throw new IllegalAuthorityException("You are not allowed to book this service.");
            }
        });
        return payments.save(paymentRecords);
    }
}
