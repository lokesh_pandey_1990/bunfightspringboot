package com.bunfight.controller.bfsservices.hallService;

import com.bunfight.common.apierrors.IllegalAuthorityException;
import com.bunfight.common.sessionandtokens.UserSessionComponent;
import com.bunfight.common.sessiondetails.GetDetailsFromSessionId;
import com.bunfight.model.bfsservices.hallService.HallServicesCancelledModel;
import com.bunfight.services.bfsservices.hallService.HallServicesCancelled;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.Collection;

/*
Author - Lokesh Pandey
*/

@RestController
public class HallServiceCancelController {

    @Autowired
    HallServicesCancelled servicesCancelled;

    @PostMapping("/bfs/v1/cancel/service")
    public HallServicesCancelledModel cancelService(HttpServletRequest request,
                                                    @RequestBody HallServicesCancelledModel cancelledModel) {
        GetDetailsFromSessionId getDetailsFromSessionId = new GetDetailsFromSessionId();
        UserSessionComponent user = getDetailsFromSessionId.getSessionDetails(request);
        Collection<GrantedAuthority> authority = user.getAuthorities();
        authority.forEach(auth -> {
            if(!auth.getAuthority().equals("ROLE_END_USER")) {
                throw new IllegalAuthorityException("You are not allowed to cancel this service.");
            }
        });
        return servicesCancelled.save(cancelledModel);
    }
}
