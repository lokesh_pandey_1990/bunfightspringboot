package com.bunfight.controller.bfsservices.hallService;

import com.bunfight.model.bfsservices.hallService.HallServiceBookedModel;
import com.bunfight.services.bfsservices.hallService.HallServiceBooked;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

/*
Author - Lokesh Pandey
*/

@RestController
public class ServiceBookedController {
    private static final Logger serviceLogger = LoggerFactory.getLogger(ServiceBookedController.class);

    @Autowired
    HallServiceBooked services;

    /*TODO
        *  Booking must be happen after the payment. This is just to test the booking service.
    */

    @PostMapping("/bfs/v1/book/service")
    HallServiceBookedModel bookService(HallServiceBookedModel serviceModel) {
        serviceLogger.info("Service booked:: " + serviceModel.toString());
        return services.save(serviceModel);
    }
}
