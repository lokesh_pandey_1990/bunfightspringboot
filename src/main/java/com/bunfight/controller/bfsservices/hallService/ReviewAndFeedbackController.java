package com.bunfight.controller.bfsservices.hallService;

import com.bunfight.common.apierrors.IllegalAuthorityException;
import com.bunfight.common.sessionandtokens.UserSessionComponent;
import com.bunfight.common.sessiondetails.GetDetailsFromSessionId;
import com.bunfight.model.bfsservices.hallService.HallServiceReviewAndFeedbackModel;
import com.bunfight.services.bfsservices.hallService.HallServiceReviewAndFeedback;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.Collection;

/*
Author - Lokesh Pandey
*/

@RestController
public class ReviewAndFeedbackController {
    private static final Logger reviewLogger = LoggerFactory.getLogger(ReviewAndFeedbackController.class);

    @Autowired
    HallServiceReviewAndFeedback reviews;

    @PostMapping("/bfs/v1/review/and/feedback/service")
    public HallServiceReviewAndFeedbackModel reviewAndFeedback(HttpServletRequest request,
                                            @RequestBody HallServiceReviewAndFeedbackModel reviewModel) throws IllegalAuthorityException {
        GetDetailsFromSessionId getDetailsFromSessionId = new GetDetailsFromSessionId();
        UserSessionComponent user = getDetailsFromSessionId.getSessionDetails(request);
        Collection<GrantedAuthority> authority = user.getAuthorities();
        authority.forEach(auth -> {
            if(!auth.getAuthority().equals("ROLE_END_USER")) {
                throw new IllegalAuthorityException("You are not allowed to review.");
            }
        });
        reviewLogger.info("Reviewed:: " + reviewModel.toString());
        return reviews.save(reviewModel);
    }
}
