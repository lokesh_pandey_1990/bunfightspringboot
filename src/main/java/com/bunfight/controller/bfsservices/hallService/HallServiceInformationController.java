package com.bunfight.controller.bfsservices.hallService;

import com.bunfight.common.apierrors.AlreadyExistsException;
import com.bunfight.common.apierrors.IllegalAuthorityException;
import com.bunfight.common.apierrors.SessionExpiredException;
import com.bunfight.common.sessionandtokens.UserSessionComponent;
import com.bunfight.common.sessiondetails.GetDetailsFromSessionId;
import com.bunfight.model.bfsservices.hallService.HallServiceInformationTable;
import com.bunfight.services.bfsservices.hallService.HallServiceInformation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.Collection;
import java.util.UUID;

/*
Author - Lokesh Pandey
*/

@RestController
public class HallServiceInformationController {
    private static final Logger hallServiceLogger = LoggerFactory.getLogger(HallServiceInformationController.class);

    @Autowired
    HallServiceInformation hallServiceInformation;

    @PutMapping("/bfs/v1/update/hall/service")
    public HallServiceInformationTable saveHallServiceInformation(HttpServletRequest request,
            @RequestBody HallServiceInformationTable hallServiceInformationTable) throws SessionExpiredException, AlreadyExistsException {
        GetDetailsFromSessionId getDetailsFromSessionId = new GetDetailsFromSessionId();
        UserSessionComponent user = getDetailsFromSessionId.getSessionDetails(request);
        Collection<GrantedAuthority> authority = user.getAuthorities();
        authority.forEach(auth -> {
            if(!auth.getAuthority().equals("ROLE_SERVICE_PROVIDER")) {
                throw new IllegalAuthorityException("You are not authorised to perform this action.");
            }
        });
        hallServiceInformationTable.setHallServiceUuid(UUID.fromString(user.getUserUuid()));
        if(checkDuplicateEntity(hallServiceInformationTable)) {
            throw new AlreadyExistsException("Phone number or email Id is already registered with other hall service.");
        }
        hallServiceLogger.info(hallServiceInformationTable.toString());
        return hallServiceInformation.save(hallServiceInformationTable);
    }

    private boolean checkDuplicateEntity(HallServiceInformationTable hallServiceInformationTable) {
        HallServiceInformationTable service = hallServiceInformation.findByHallServiceEmailId(hallServiceInformationTable.getHallServiceEmailId());
        HallServiceInformationTable serviceInfo = hallServiceInformation.findByHallServicePhoneNumber(hallServiceInformationTable.getHallServicePhoneNumber());
        HallServiceInformationTable serviceInfo1 = hallServiceInformation.findByHallServiceUuid(hallServiceInformationTable.getHallServiceUuid());
        if(service == null && serviceInfo == null && serviceInfo1 == null) {
            return false;
        }
        if(!service.getHallServiceUuid().equals(hallServiceInformationTable.getHallServiceUuid())
                || !serviceInfo.getHallServiceUuid().equals(hallServiceInformationTable.getHallServiceUuid())
                || !serviceInfo1.getHallServiceUuid().equals(hallServiceInformationTable.getHallServiceUuid())) {
            return true;
        }
        return false;
    }
}
