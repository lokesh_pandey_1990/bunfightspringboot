package com.bunfight.controller.bfsservices.hallService;

import com.bunfight.common.apierrors.SessionExpiredException;
import com.bunfight.common.apierrors.UploadingImageFailedException;
import com.bunfight.common.sessionandtokens.UserSessionComponent;
import com.bunfight.common.sessiondetails.GetDetailsFromSessionId;
import com.bunfight.common.simplestorageservice.BunfightSimpleStorageService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.net.URL;

/*
Author - Lokesh Pandey
*/

@RestController
public class HallServiceMultimediaUploader {
    private static final Logger bfsHallLogger = LoggerFactory.getLogger(HallServiceMultimediaUploader.class);

    public void publishImages(HttpServletRequest request,
                              @RequestParam("file") MultipartFile image) throws SessionExpiredException, IOException {
        BunfightSimpleStorageService storageService = new BunfightSimpleStorageService();
        GetDetailsFromSessionId getDetailsFromSessionId = new GetDetailsFromSessionId();
        UserSessionComponent user = getDetailsFromSessionId.getSessionDetails(request);
        String uuid = user.getUserUuid();
        if(uuid == null) {
            throw new SessionExpiredException("Please login again.");
        }
        URL url = storageService.uploadImages(image, uuid);
        if(url == null) {
            throw new UploadingImageFailedException("Oops! Something went wrong. It's not you, It's us. Please try again");
        } else {

        }
    }

    public void uploadProfilePhoto(HttpServletRequest request,
                                   @RequestParam("file") MultipartFile image) {
        /*TO DO*/
    }

    public void publishVideos(HttpServletRequest request,
                                   @RequestParam("file") MultipartFile image) {
        /*TO DO*/
    }
}
