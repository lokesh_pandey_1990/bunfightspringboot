package com.bunfight.configuration.authConfig;

import com.bunfight.configuration.CommonDbConfiguration;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;
import java.util.HashMap;
import java.util.Map;

/*
Author - Lokesh Pandey
*/

@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(
        entityManagerFactoryRef = "authenticationEntityManagerFactory",
        transactionManagerRef = "authenticationTransactionManager",
        basePackages = "com.bunfight.services.authenticate")
public class AuthenticationDbConfigImpl implements CommonDbConfiguration {

    @Override
    @Primary
    @Bean(name = "authDataSource")
    @ConfigurationProperties(prefix = "spring.auth.datasource")
    public DataSource createDataSource() {
        return DataSourceBuilder.create().build();
    }

    @Override
    @Primary
    @Bean(name = "authenticationEntityManagerFactory")
    public LocalContainerEntityManagerFactoryBean entityManagerFactoryBean(EntityManagerFactoryBuilder builder,
                                                                           @Qualifier("authDataSource") DataSource dataSource) {
        Map<String, Object> dbProperties = new HashMap<>();
        dbProperties.put("hibernate.hdm2ddl.auto", "validate");
        dbProperties.put("hibernate.dialect", "org.hibernate.dialect.PostgreSQLDialect");
        return builder.dataSource(dataSource).properties(dbProperties).packages("com.bunfight.model.authentication").
                persistenceUnit("Authentication").build();
    }

    @Override
    @Primary
    @Bean(name = "authenticationTransactionManager")
    public PlatformTransactionManager transactionManager(@Qualifier("authenticationEntityManagerFactory")
                                                                     EntityManagerFactory entityManagerFactory) {
        return new JpaTransactionManager(entityManagerFactory);
    }
}
