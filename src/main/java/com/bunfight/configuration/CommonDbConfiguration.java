package com.bunfight.configuration;

import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

/*
Author - Lokesh Pandey
*/

public interface CommonDbConfiguration {
    public DataSource createDataSource();
    public LocalContainerEntityManagerFactoryBean entityManagerFactoryBean(EntityManagerFactoryBuilder builder,
                                                                           DataSource dataSource);
    public PlatformTransactionManager transactionManager(EntityManagerFactory entityManagerFactory);
}
