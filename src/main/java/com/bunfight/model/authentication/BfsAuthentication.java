package com.bunfight.model.authentication;

import com.bunfight.common.datetime.LocalDateTimeConverter;
import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.time.LocalDateTime;
import java.util.UUID;

/*
Author - Lokesh Pandey
*/

@Data
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
@Entity
@Table(name = "bfs_user_auth_table")
public class BfsAuthentication {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @NotNull
    @Column(name = "user_uuid", unique = true)
    private UUID userUuid;
    @NotNull
    @NotBlank
    @Column(name = "user_full_name")
    private String userFullName;
    @NotNull
    @NotBlank
    @Email
    @Column(name = "user_email_id", unique = true)
    private String userEmailId;
    @NotNull
    @Positive
    @Column(name = "user_phone_number", unique = true)
    private Long userPhoneNumber;
    @NotNull
    @NotBlank
    @Column(name = "user_password")
    private String userPassword;
    @NotNull
    @Column(name = "user_registration_date")
    @Convert(converter = LocalDateTimeConverter.class)
    private LocalDateTime userRegistrationDate;
    @NotNull
    @Column(name = "user_service_provider")
    private boolean isUserServiceProvider;
    @NotNull
    @Column(name = "user_email_verified")
    private boolean isUserEmailVerified;
    @NotNull
    @Column(name = "user_phone_number_verified")
    private boolean isUserPhoneNumberVerified;
}
