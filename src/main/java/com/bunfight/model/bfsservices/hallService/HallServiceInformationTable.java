package com.bunfight.model.bfsservices.hallService;

import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import java.util.UUID;

/*
Author - Lokesh Pandey
*/

@Data
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
@Entity
@Table(name = "bfs_hall_service_information_table")
public class HallServiceInformationTable {
    @Id
    @NotNull
    @Column(name = "hall_service_uuid", unique = true)
    private UUID hallServiceUuid;
    @NotNull
    @NotBlank
    @Column(name = "hall_service_address")
    private String hallServiceAddress;
    @NotNull
    @NotBlank
    @Column(name = "hall_service_city")
    private String hallServiceCity;
    @NotNull
    @Column(name = "hall_service_latitude")
    private Double hallServiceLatitude;
    @NotNull
    @Column(name = "hall_service_longitude")
    private Double hallServiceLongitude;
    @NotNull
    @NotBlank
    @Column(name = "hall_service_location_landmark")
    private String hallServiceLocationLandmark;
    @NotNull
    @Email
    @Column(name = "hall_service_email_id", unique = true)
    private String hallServiceEmailId;
    @NotNull
    @NotBlank
    @Column(name = "hall_name")
    private String hallName;
    @NotNull
    @Positive
    @Column(name = "hall_service_phone_number", unique = true)
    private Long hallServicePhoneNumber;
    @NotNull
    @Column(name = "hall_service_pin_code")
    private Long hallServicePinCode;
    @NotNull
    @Column(name = "hall_service_property_verified")
    private boolean hallServicePropertyVerified;
    @NotNull
    @NotBlank
    @Column(name = "hall_service_state")
    private String hallServiceState;
    @NotNull
    @Column(name = "hall_service_cost_per_day_excluding_services")
    private Double hallServiceCostPerDayExcludingServices;
    @NotNull
    @Column(name = "hall_service_weighted_rating")
    private Double hallServiceWeightedRating;
    @NotNull
    @Column(name = "hall_service_other_services_free")
    private String hallServiceOtherServiceFree;
    @NotNull
    @Column(name = "hall_service_capacity")
    private int hallServiceCapacity;
    @NotNull
    @Column(name = "hall_service_accommodations_provided")
    private boolean hallServiceAccommodationsProvided;
    @NotNull
    @Column(name = "hall_service_accommodations_charge")
    private Double hallServiceAccommodationsCharge;
    @NotNull
    @Column(name = "hall_service_ac_non_ac_or_both")
    private String hallServiceAcNonAcOrBoth;
    @NotNull
    @Column(name = "hall_service_ac_charge")
    private Double hallServiceAcCharge;
    @NotNull
    @Column(name = "hall_service_non_ac_charge")
    private Double hallServiceNonAcCharge;
    @NotNull
    @Column(name = "hall_service_transport_service_provided")
    private boolean hallServiceTransportServiceProvided;
    @NotNull
    @Column(name = "hall_service_transport_service_charge")
    private Double hallServiceTransportServiceCharge;
    @NotNull
    @Column(name = "hall_service_garden")
    private boolean hallServiceGarden;
    @NotNull
    @Column(name = "hall_service_best_for")
    private String hallServiceBestFor;
    @NotNull
    @Column(name = "hall_service_provide_dining")
    private boolean hallServiceProvideDining;
    @NotNull
    @Column(name = "hall_service_provide_decoration")
    private boolean hallServiceProvideDecoration;
    @NotNull
    @Column(name = "hall_service_decoration_cost")
    private Double hallServiceDecorationCost;
    @NotNull
    @Column(name = "hall_service_has_parking")
    private boolean hallServiceHasParking;
    @NotNull
    @Column(name = "hall_service_parking_charge")
    private Double hallServiceParkingCharge;
    @NotNull
    @Column(name = "hall_service_electricity_bill_charge")
    private Double hallServiceElectricityBillCharge;
    @NotNull
    @Column(name = "hall_service_provide_photography")
    private boolean hallServiceProvidePhotography;
    @NotNull
    @Column(name = "hall_service_photography_cost")
    private Double hallServicePhotographyCost;
    @NotNull
    @Column(name = "provide_make_up_facility")
    private boolean provideMakeUpFacility;
}
