package com.bunfight.model.bfsservices.hallService;

import com.bunfight.common.datetime.LocalDateTimeConverter;
import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import java.time.LocalDateTime;

/*
Author - Lokesh Pandey
*/

@Data
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
@Entity
@Table(name = "bfs_hall_service_reviews_and_feedback_table")
public class HallServiceReviewAndFeedbackModel {
    @Id
    @NotNull
    @NotBlank
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    @NotNull
    @NotBlank
    @Column(name = "end_user_uuid")
    private String endUserUuid;
    @NotNull
    @NotBlank
    @Column(name = "service_uuid")
    private String serviceUuid;
    @Positive
    @Column(name = "service_five_star_rating")
    private int serviceFiveStarRating;
    @Positive
    @Column(name = "service_four_star_rating")
    private int serviceFourStarRating;
    @Positive
    @Column(name = "service_three_star_rating")
    private int serviceThreeStarRating;
    @Positive
    @Column(name = "service_two_star_rating")
    private int serviceTwoStarRating;
    @Positive
    @Column(name = "service_one_star_rating")
    private int serviceOneStarRating;
    @NotNull
    @NotBlank
    @Column(name = "service_feedback")
    private String serviceFeedback;
    @NotNull
    @NotBlank
    @Column(name = "bfs_company_feedback")
    private String bfsFeedback;
    @NotNull
    @NotBlank
    @Column(name = "service_reviewed_date")
    @Convert(converter = LocalDateTimeConverter.class)
    private LocalDateTime serviceReviewedDate;
}
