package com.bunfight.model.bfsservices.hallService;

import com.bunfight.common.datetime.LocalDateTimeConverter;
import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.UUID;

/*
Author - Lokesh Pandey
*/

@Data
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
@Entity
@Table(name = "bfs_hall_services_cancelled")
public class HallServicesCancelledModel {
    @Id
    @NotNull
    @NotBlank
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "cancelled_id")
    private UUID cancelledId;
    @NotNull
    @Column(name = "cancelled_already_booking_id")
    private String cancelledAlreadyBookingId;
    @NotNull
    @NotBlank
    @Column(name = "end_user_uuid")
    private String endUserUuid;
    @NotNull
    @NotBlank
    @Column(name = "service_uuid")
    private String serviceUuid;
    @NotNull
    @NotBlank
    @Column(name = "service_cancel_reason")
    private String serviceCancelReason;
    @NotNull
    @NotBlank
    @Column(name = "service_cancelled_date")
    @Convert(converter = LocalDateTimeConverter.class)
    private LocalDateTime serviceCancelledDate;
}
