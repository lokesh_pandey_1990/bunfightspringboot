package com.bunfight.model.bfsservices.hallService;

import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.UUID;

/*
Author - Lokesh Pandey
*/

@Data
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
@Entity
@Table(name = "bfs_hall_service_payment_table")
public class HallServicePaymentRecords {
    @Id
    @NotNull
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "booking_id", unique = true)
    private UUID bookingId;
    @NotNull
    @Column(name = "token_amount")
    private Double tokenAmount;
    @NotNull
    @Column(name = "token_amount_paid")
    private boolean tokenAmountPaid;
    @NotNull
    @Column(name = "electricity_amount")
    private Double electricityAmount;
    @NotNull
    @Column(name = "electricity_amount_paid")
    private boolean electricityAmountPaid;
    @NotNull
    @Column(name = "full_amount")
    private Double fullAmount;
    @NotNull
    @Column(name = "full_amount_paid")
    private boolean fullAmountPaid;
}
