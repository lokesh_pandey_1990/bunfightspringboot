package com.bunfight.model.bfsservices.hallService;

import com.bunfight.common.datetime.LocalDateTimeConverter;
import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.UUID;

/*
Author - Lokesh Pandey
*/

@Data
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
@Entity
@Table(name = "bfs_hall_services_booked")
public class HallServiceBookedModel {
    @Id
    @NotNull
    @NotBlank
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "booking_id")
    private UUID bookingId;
    @NotNull
    @NotBlank
    @Column(name = "end_user_uuid")
    private String endUserUuid;
    @NotNull
    @NotBlank
    @Column(name = "service_booked_uuid")
    private String serviceBookedUuid;
    @NotNull
    @NotBlank
    @Column(name = "service_booked_date")
    @Convert(converter = LocalDateTimeConverter.class)
    private LocalDateTime serviceBookedDate;
    @NotNull
    @NotBlank
    @Column(name = "service_booked_for")
    private String serviceBookedFor;
    @NotNull
    @Column(name = "number_of_days_service_booked")
    private int numberOfDaysServiceBooked;
    @NotNull
    @NotBlank
    @Column(name = "service_booked_from_date")
    @Convert(converter = LocalDateTimeConverter.class)
    private LocalDateTime serviceBookedFromDate;
    @NotNull
    @NotBlank
    @Column(name = "service_booked_to_date")
    @Convert(converter = LocalDateTimeConverter.class)
    private LocalDateTime serviceBookedToDate;
    @NotNull
    @Column(name = "services_using")
    private String servicesUsing;
    @NotNull
    @Column(name = "total_cost")
    private Double totalCost;
    @NotNull
    @Column(name = "service_booked")
    private boolean serviceBooked;
    @NotNull
    @Column(name = "paid_at_time_of_booking")
    private Double paidAtTimeOfBooking;
    @NotNull
    @Column(name = "balance_amount")
    private Double balanceAmount;
}
