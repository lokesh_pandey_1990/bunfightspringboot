package com.bunfight.model.sms;

import lombok.*;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
public class SMSModel {
    private String phoneNumber;
    private String textMessage;
}
