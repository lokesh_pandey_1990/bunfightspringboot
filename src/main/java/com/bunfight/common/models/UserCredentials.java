package com.bunfight.common.models;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

/*
Author - Lokesh Pandey
*/

@Getter
@Setter
@AllArgsConstructor
public class UserCredentials {
    private String userLoginWay;
    private String password;
}
