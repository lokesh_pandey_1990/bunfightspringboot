package com.bunfight.common.models;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

/*
Author - Lokesh Pandey
*/

@Getter
@Setter
@AllArgsConstructor
public class SearchControllerModel {
    private Double latitude;
    private Double longitude;
    private String cityName;
    private String serviceNeededFor;
    private String fromDate;
    private String toDate;
}
