package com.bunfight.common.generate;

import com.bunfight.common.passwordhandler.PasswordHashingSalt;
import org.passay.CharacterData;
import org.passay.CharacterRule;
import org.passay.EnglishCharacterData;
import org.passay.PasswordGenerator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;

/*
Author - Lokesh Pandey
*/

public class OnRegistrationGeneratePassword {
    private final static Logger regLogger = LoggerFactory.getLogger(OnRegistrationGeneratePassword.class);

    public static Map<String, String> generateRandomAndHashedPassword() {
        Map<String, String> passwordList = new HashMap<>();
        String readablePassword = generateBfsUserPassword();
        regLogger.info("Generated password is:: " + readablePassword);
        passwordList.put("readablePassword", readablePassword);
        PasswordHashingSalt hashingPassword = new PasswordHashingSalt();
        String hashedPassword = hashingPassword.bcryptEncodedPassword(readablePassword);
        passwordList.put("hashedPassword", hashedPassword);
        return passwordList;
    }

    private static String generateBfsUserPassword() {
        PasswordGenerator passwordGenerator = new PasswordGenerator();
        CharacterRule alphabet = new CharacterRule(EnglishCharacterData.Alphabetical);
        CharacterRule digits = new CharacterRule(EnglishCharacterData.Digit);
        CharacterRule specialCharacter = new CharacterRule(new CharacterData() {
            @Override
            public String getErrorCode() {
                return "ERROR_CODE";
            }

            @Override
            public String getCharacters() {
                return "%$#@!&*()";
            }
        });
        String readablePassword = passwordGenerator.generatePassword(8, alphabet, digits, specialCharacter);
        return readablePassword;
    }
}
