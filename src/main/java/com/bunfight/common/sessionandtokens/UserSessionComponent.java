package com.bunfight.common.sessionandtokens;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;

import java.util.Collection;

/*
Author - Lokesh Pandey
*/

@Getter
@Setter
@ToString
public class UserSessionComponent extends User {

    public UserSessionComponent(String username, String password, boolean enabled, boolean accountNonExpired,
                                boolean credentialsNonExpired, boolean accountNonLocked,
                                Collection<? extends GrantedAuthority> authorities) {
        super(username, password, enabled, accountNonExpired, credentialsNonExpired, accountNonLocked, authorities);
    }
    private String userUuid;
    private String userFullName;
    private String userEmailId;
    private Long userPhoneNumber;
    private boolean emailIdVerified;
}
