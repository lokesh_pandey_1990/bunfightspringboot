package com.bunfight.common.amazonconnection;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSStaticCredentialsProvider;

/*
Author - Lokesh Pandey
*/

public class AmazonEndPointConnection {
    private final static String AWS_ACCESS_KEY_ID = "RANDOM_AWS_ACCESS_KEY_ID";
    private final static String AWS_SECRET_KEY = "RANDOM_AWS_SECRET_KEY";

    public AWSStaticCredentialsProvider getAWSStaticCredentials() {
        return new AWSStaticCredentialsProvider(new AWSCredentials() {
            @Override
            public String getAWSAccessKeyId() {
                return AWS_ACCESS_KEY_ID;
            }

            @Override
            public String getAWSSecretKey() {
                return AWS_SECRET_KEY;
            }
        });
    }
}
