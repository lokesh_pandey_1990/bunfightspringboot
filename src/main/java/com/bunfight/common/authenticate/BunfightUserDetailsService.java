package com.bunfight.common.authenticate;

import com.bunfight.common.apierrors.EntityNotFoundException;
import com.bunfight.common.sessionandtokens.UserSessionComponent;
import com.bunfight.common.validation.CommonValidations;
import com.bunfight.model.authentication.BfsAuthentication;
import com.bunfight.services.authenticate.BfsAuthService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/*
Author - Lokesh Pandey
*/

@Service
public class BunfightUserDetailsService implements UserDetailsService {
    private static final String SERVICE_PROVIDER = "ROLE_SERVICE_PROVIDER";
    private static final String END_USER = "ROLE_END_USER";

    @Autowired
    BfsAuthService bfsAuthService;

    @Override
    public UserDetails loadUserByUsername(String userLoginWay) throws UsernameNotFoundException {
        CommonValidations validate = new CommonValidations();
        if(userLoginWay != null && validate.validateEmailId(userLoginWay)) {
            BfsAuthentication user = bfsAuthService.findByUserEmailId(userLoginWay);
            if(user == null || user.getUserUuid() == null) {
                throw new EntityNotFoundException("Please check your credentials.");
            }
            List<GrantedAuthority> authorityList = getUsersAuthorization(user.isUserServiceProvider());
            return buildUserForAuthentication(user, authorityList);
        } else if(userLoginWay != null && validate.validatePhoneNumber(userLoginWay)) {
            BfsAuthentication user = bfsAuthService.findByUserPhoneNumber(Long.parseLong(userLoginWay));
            if(user == null || user.getUserUuid() == null) {
                throw new EntityNotFoundException("Please check your credentials.");
            }
            List<GrantedAuthority> authorityList = getUsersAuthorization(user.isUserServiceProvider());
            return buildUserForAuthentication(user, authorityList);
        }
        return null;
    }

    private User buildUserForAuthentication(BfsAuthentication user, List<GrantedAuthority> authorityList) {
        UserSessionComponent currentUser = new UserSessionComponent(user.getUserEmailId(), user.getUserPassword(),
                true, true, true,
                true, authorityList);
        currentUser.setUserUuid(user.getUserUuid().toString());
        currentUser.setUserFullName(user.getUserFullName());
        currentUser.setUserEmailId(user.getUserEmailId());
        currentUser.setUserPhoneNumber(user.getUserPhoneNumber());
        currentUser.setEmailIdVerified(user.isUserEmailVerified());
        return currentUser;
    }

    private List<GrantedAuthority> getUsersAuthorization(@NotNull @NotBlank boolean getUserServiceProvider) {
        Set<GrantedAuthority> setAuth = new HashSet<>();
        if(getUserServiceProvider) {
            setAuth.add(new SimpleGrantedAuthority(SERVICE_PROVIDER));
        } else {
            setAuth.add(new SimpleGrantedAuthority(END_USER));
        }
        return new ArrayList<GrantedAuthority>(setAuth);
    }
}
