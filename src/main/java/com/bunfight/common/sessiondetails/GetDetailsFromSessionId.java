package com.bunfight.common.sessiondetails;

import com.bunfight.common.apierrors.SessionExpiredException;
import com.bunfight.common.sessionandtokens.UserSessionComponent;
import org.springframework.security.core.context.SecurityContext;

import javax.servlet.http.HttpServletRequest;

import static org.springframework.security.web.context.HttpSessionSecurityContextRepository.SPRING_SECURITY_CONTEXT_KEY;

/*
Author - Lokesh Pandey
*/

public class GetDetailsFromSessionId {

    public UserSessionComponent getSessionDetails(HttpServletRequest request) throws SessionExpiredException {
        String requestedSessionId = request.getRequestedSessionId();
        UserSessionComponent user = null;
        SecurityContext securityContext;
        if(requestedSessionId != null) {
            if(request.getSession().getAttribute(SPRING_SECURITY_CONTEXT_KEY) != null) {
                securityContext = (SecurityContext) request.getSession().getAttribute(SPRING_SECURITY_CONTEXT_KEY);
                user = (UserSessionComponent) securityContext.getAuthentication().getPrincipal();
            }
        }
        if(user == null) {
            throw new SessionExpiredException("Please login again");
        }
        return user;
    }
}
