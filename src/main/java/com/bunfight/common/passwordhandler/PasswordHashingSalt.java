package com.bunfight.common.passwordhandler;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import java.security.SecureRandom;

/*
Author - Lokesh Pandey
*/

public class PasswordHashingSalt {
    private static final int STRENGTH = 10;

    public String bcryptEncodedPassword(String readablePassword) {
        BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder(STRENGTH, new SecureRandom());
        String encodedPassword = bCryptPasswordEncoder.encode(readablePassword);
        return encodedPassword;
    }

    public boolean matchPlainPasswordWithEncoded(String readablePassword, String encodedPassword) {
        BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
        if(bCryptPasswordEncoder.matches(readablePassword, encodedPassword)) {
            return true;
        }
        return false;
    }
}
