package com.bunfight.common.validation;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/*
Author - Lokesh Pandey
*/

public class CommonValidations {

    public boolean validateEmailId(String emailId) {
        Pattern pattern = Pattern.compile("[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}");
        Matcher matcher = pattern.matcher(emailId);
        if(matcher.matches()) {
            return true;
        }
        return false;
    }

    public boolean validatePhoneNumber(String phoneNumber) {
        if(phoneNumber.matches("[0-9]+") && phoneNumber.length() == 10)
            return true;
        return false;
    }
}
