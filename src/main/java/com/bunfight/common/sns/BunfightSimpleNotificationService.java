package com.bunfight.common.sns;

import com.amazonaws.services.sns.AmazonSNS;
import com.amazonaws.services.sns.AmazonSNSClient;
import com.amazonaws.services.sns.model.MessageAttributeValue;
import com.amazonaws.services.sns.model.PublishRequest;
import com.amazonaws.services.sns.model.PublishResult;
import com.bunfight.common.amazonconnection.AmazonEndPointConnection;

import java.util.HashMap;
import java.util.Map;

/*
Author - Lokesh Pandey
*/

public class BunfightSimpleNotificationService {
    private static final String SENDER_ID = "Bunfight";
    private static final String SMS_TYPE = "Promotional";

    public void sendSMSMessage(String phoneNumber, String textMessage) {
        AmazonEndPointConnection amazonEndPointConnection = new AmazonEndPointConnection();
        AmazonSNS amazonSNS = AmazonSNSClient.builder()
                .withRegion("us-east-2")
                .withCredentials(amazonEndPointConnection.getAWSStaticCredentials())
                .build();
        Map<String, MessageAttributeValue> smsAttributes = new HashMap<String, MessageAttributeValue>();
        smsAttributes.put("AWS.SNS.SMS.SenderID", new MessageAttributeValue()
                .withStringValue(SENDER_ID)
                .withDataType("String"));
        smsAttributes.put("AWS.SNS.SMS.SMSType", new MessageAttributeValue()
                .withStringValue(SMS_TYPE)
                .withDataType("String"));
        PublishResult publishResult = amazonSNS.publish(new PublishRequest()
                .withMessage(textMessage)
                .withPhoneNumber(phoneNumber)
                .withMessageAttributes(smsAttributes));
        System.out.println(publishResult);
    }
}
