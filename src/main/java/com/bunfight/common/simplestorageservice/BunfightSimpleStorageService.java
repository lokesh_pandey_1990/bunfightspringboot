package com.bunfight.common.simplestorageservice;

import com.amazonaws.AmazonServiceException;
import com.amazonaws.SdkClientException;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.bunfight.common.amazonconnection.AmazonEndPointConnection;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;

/*
Author - Lokesh Pandey
*/

public class BunfightSimpleStorageService {
    private final static String BUCKET_NAME = "bfs-multimedia-buckets2020";

    public URL uploadImages(MultipartFile image, String uniqueId) throws IOException {
        File imageFile = convertMultipartFileToFile(image);
        AmazonEndPointConnection connection = new AmazonEndPointConnection();
        try {
            AmazonS3 amazonS3 = AmazonS3ClientBuilder.standard()
                    .withCredentials(connection.getAWSStaticCredentials())
                    .withRegion("ap-south-1")
                    .build();
            PutObjectRequest putObjectRequest = new PutObjectRequest(BUCKET_NAME,
                    uniqueId + "/images/" + image.getOriginalFilename(), imageFile);
            ObjectMetadata objectMetadata = new ObjectMetadata();
            objectMetadata.setContentType(image.getContentType());
            objectMetadata.setContentLength(image.getSize());
            putObjectRequest.setMetadata(objectMetadata);
            amazonS3.putObject(putObjectRequest);
            return amazonS3.getUrl(BUCKET_NAME, uniqueId + "/images/" + image.getOriginalFilename());
        } catch (AmazonServiceException e) {
            e.printStackTrace();
            throw new AmazonServiceException("Oops! Something went wrong. It's not you. It's us. Please try again");
        } catch (SdkClientException e) {
            e.printStackTrace();
            throw new SdkClientException("Oops! Something went wrong. It's not you. It's us. Please try again");
        }
    }

    private File convertMultipartFileToFile(MultipartFile image) throws IOException {
        File imageConvertedFile = new File(image.getOriginalFilename());
        FileOutputStream fos = new FileOutputStream(imageConvertedFile);
        fos.write(image.getBytes());
        fos.close();
        return imageConvertedFile;
    }
}