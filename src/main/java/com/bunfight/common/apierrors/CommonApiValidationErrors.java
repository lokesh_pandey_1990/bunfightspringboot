package com.bunfight.common.apierrors;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;

/*
Author - Lokesh Pandey
*/

@Data
@EqualsAndHashCode(callSuper = false)
@AllArgsConstructor
public class CommonApiValidationErrors extends CommonApiSubError {
    private String object;
    private String field;
    private Object rejectedValue;
    private String message;

    CommonApiValidationErrors(String object, String message) {
        this.object = object;
        this.message = message;
    }
}
