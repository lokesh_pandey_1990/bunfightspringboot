package com.bunfight.common.apierrors;

/*
Author - Lokesh Pandey
*/

public class IllegalAuthorityException extends RuntimeException {
    private static final Long serialVersionUID = 1L;
    public IllegalAuthorityException(String message) {
        super(message);
    }
}
