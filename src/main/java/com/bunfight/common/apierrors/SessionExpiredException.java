package com.bunfight.common.apierrors;

/*
Author - Lokesh Pandey
*/

public class SessionExpiredException extends RuntimeException {
    private static final Long serialVersionUID = 1L;
    public SessionExpiredException(String message) {
        super(message);
    }
}
