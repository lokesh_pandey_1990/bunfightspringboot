package com.bunfight.common.apierrors;

/*
Author - Lokesh Pandey
*/

public class EntityNotFoundException extends RuntimeException {
    private static final Long serialVersionUID = 1L;
    public EntityNotFoundException(String message) {
        super(message);
    }

}
