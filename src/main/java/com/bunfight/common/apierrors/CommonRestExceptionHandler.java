package com.bunfight.common.apierrors;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;

import java.time.LocalDateTime;

/*
Author - Lokesh Pandey
*/

@ControllerAdvice
public class CommonRestExceptionHandler  {

    /*Handle entity not found exception*/
    @ExceptionHandler(EntityNotFoundException.class)
    public ResponseEntity<?> handleEntityNotFoundException(EntityNotFoundException exception, WebRequest request) {
        CommonApiErrors commonApiErrors = new CommonApiErrors(LocalDateTime.now(), exception.getMessage(), request.getDescription(false));
        return new ResponseEntity(commonApiErrors, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(AlreadyExistsException.class)
    public ResponseEntity<?> handleUserAlreadyExistsException(AlreadyExistsException exception,
                                                              WebRequest request) {
        CommonApiErrors commonApiErrors = new CommonApiErrors(LocalDateTime.now(), exception.getMessage(),
                request.getDescription(false));
        return new ResponseEntity(commonApiErrors, HttpStatus.CONFLICT);
    }

    @ExceptionHandler(UploadingImageFailedException.class)
    public ResponseEntity<?> handleInvalidImageException(UploadingImageFailedException exception,
                                                         WebRequest request) {
        CommonApiErrors commonApiErrors = new CommonApiErrors(LocalDateTime.now(), exception.getMessage(),
                request.getDescription(false));
        return new ResponseEntity(commonApiErrors, HttpStatus.EXPECTATION_FAILED);
    }


    @ExceptionHandler(SessionExpiredException.class)
    public ResponseEntity<?> handleSessionExpiredException(SessionExpiredException exception,
                                                         WebRequest request) {
        CommonApiErrors commonApiErrors = new CommonApiErrors(LocalDateTime.now(), exception.getMessage(),
                request.getDescription(false));
        return new ResponseEntity(commonApiErrors, HttpStatus.FORBIDDEN);
    }

    @ExceptionHandler(IllegalAuthorityException.class)
    public ResponseEntity<?> handleIllegalAuthorityException(IllegalAuthorityException exception,
                                                           WebRequest request) {
        CommonApiErrors commonApiErrors = new CommonApiErrors(LocalDateTime.now(), exception.getMessage(),
                request.getDescription(false));
        return new ResponseEntity(commonApiErrors, HttpStatus.FORBIDDEN);
    }
}
