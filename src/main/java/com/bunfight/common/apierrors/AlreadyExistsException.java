package com.bunfight.common.apierrors;

/*
Author - Lokesh Pandey
*/

public class AlreadyExistsException extends RuntimeException {
    private static final Long serialVersionUID = 1L;
    public AlreadyExistsException(String message) {
        super(message);
    }
}
