package com.bunfight.common.apierrors;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.*;
import org.springframework.http.HttpStatus;

import java.time.LocalDateTime;
import java.util.List;

/*
Author - Lokesh Pandey
*/

@Data
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
public class CommonApiErrors {
    private HttpStatus status;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy hh:mm:ss")
    private LocalDateTime timestamp;
    private String message;
    private String description;
    private List<CommonApiSubError> subErrors;

    public CommonApiErrors(LocalDateTime date, String message, String description) {
        this.timestamp = date;
        this.message = message;
        this.description = description;
    }
}
