package com.bunfight.common.apierrors;

/*
Author - Lokesh Pandey
*/

public class UploadingImageFailedException extends RuntimeException {
    private static final Long serialVersionUID = 1L;
    public UploadingImageFailedException(String message) {
        super(message);
    }
}
