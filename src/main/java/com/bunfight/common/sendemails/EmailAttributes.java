package com.bunfight.common.sendemails;

import lombok.*;

import java.util.List;

/*
Author - Lokesh Pandey
*/

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
public class EmailAttributes {
    private String fromEmailAddress;
    private List<String> toEmailAddresses;
    private List<String> ccEmailAddresses;
    private List<String> bccEmailAddresses;
    private String subject;
    private String body;
}
