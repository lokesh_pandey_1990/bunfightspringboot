package com.bunfight.common.sendemails;

import com.amazonaws.services.simpleemail.AmazonSimpleEmailService;
import com.amazonaws.services.simpleemail.AmazonSimpleEmailServiceClientBuilder;
import com.amazonaws.services.simpleemail.model.*;
import com.bunfight.common.amazonconnection.AmazonEndPointConnection;

/*
Author - Lokesh Pandey
*/

public class SendingEmails {

    public void sendEmails(EmailAttributes emailAttributes) {
        AmazonEndPointConnection amazonEndPointConnection = new AmazonEndPointConnection();
        AmazonSimpleEmailService client = AmazonSimpleEmailServiceClientBuilder.standard()
            .withCredentials(amazonEndPointConnection.getAWSStaticCredentials())
            .withRegion("us-east-2").build();
        SendEmailRequest request = new SendEmailRequest()
            .withSource("786lokeshpandey@gmail.com")
            .withDestination(new Destination().withToAddresses(emailAttributes.getToEmailAddresses())
                .withCcAddresses(emailAttributes.getCcEmailAddresses())
                .withBccAddresses(emailAttributes.getBccEmailAddresses()))
            .withMessage(
                new Message()
                    .withSubject(new Content().withData(emailAttributes.getSubject()).withCharset("UTF-8"))
                    .withBody(
                        new Body()
                            .withText(new Content().withData(emailAttributes.getBody()).withCharset("UTF-8"))));
        SendEmailResult response = client.sendEmail(request);
        System.out.println(response.getMessageId());
    }
}
