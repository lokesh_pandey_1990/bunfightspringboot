package com.bunfight.services.bfsservices.hallService;

import com.bunfight.model.bfsservices.hallService.HallServiceReviewAndFeedbackModel;
import org.springframework.data.repository.CrudRepository;

/*
Author - Lokesh Pandey
*/

public interface HallServiceReviewAndFeedback extends CrudRepository<HallServiceReviewAndFeedbackModel, String> {
}
