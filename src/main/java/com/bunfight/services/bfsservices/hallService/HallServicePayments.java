package com.bunfight.services.bfsservices.hallService;

import com.bunfight.model.bfsservices.hallService.HallServicePaymentRecords;
import org.springframework.data.repository.CrudRepository;

/*
Author - Lokesh Pandey
*/

public interface HallServicePayments extends CrudRepository<HallServicePaymentRecords, String> {
}
