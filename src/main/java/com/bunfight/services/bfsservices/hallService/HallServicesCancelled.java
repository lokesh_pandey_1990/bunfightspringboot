package com.bunfight.services.bfsservices.hallService;

import com.bunfight.model.bfsservices.hallService.HallServicesCancelledModel;
import org.springframework.data.repository.CrudRepository;

/*
Author - Lokesh Pandey
*/

public interface HallServicesCancelled extends CrudRepository<HallServicesCancelledModel, String> {
}
