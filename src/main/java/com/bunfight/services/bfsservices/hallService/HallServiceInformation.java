package com.bunfight.services.bfsservices.hallService;

import com.bunfight.model.bfsservices.hallService.HallServiceInformationTable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.UUID;

/*
Author - Lokesh Pandey
*/

public interface HallServiceInformation extends CrudRepository<HallServiceInformationTable, String> {
    HallServiceInformationTable findByHallServiceEmailId(String emailId);
    HallServiceInformationTable findByHallServicePhoneNumber(Long phoneNumber);
    HallServiceInformationTable findByHallServiceUuid(UUID serviceUuid);

    @Query(value = "SELECT * FROM bfs_hall_service_information_table WHERE hall_service_city=?1 AND \n" +
        "hall_service_property_verified=true AND hall_service_best_for LIKE %?2% AND \n" +
        "hall_service_uuid NOT IN (SELECT service_booked_uuid\\:\\:uuid FROM bfs_hall_services_booked WHERE \n" +
        "service_booked_from_date BETWEEN ?3\\:\\:date AND ?4\\:\\:date AND \n" +
        "service_booked_to_date BETWEEN ?3\\:\\:date AND ?4\\:\\:date  AND service_booked!=true)",
        nativeQuery = true)
    List<HallServiceInformationTable> findByHallServiceCity(String city, String serviceNeededFor,
                                   String fromDate, String toDate);

    @Query(value = "SELECT *\n" +
        "FROM (\n" +
        "  SELECT *, \n" +
        "        ( 6371 * acos( cos( radians(?1) ) * cos( radians( hall_service_latitude ) ) * \n" +
        "          cos( radians( hall_service_longitude ) - radians(?2) ) + sin( radians(?1) ) * \n" +
        "          sin( radians( hall_service_latitude ) ) ) ) AS distance \n" +
        "  from bfs_hall_service_information_table t1\n" +
        "  WHERE hall_service_property_verified = true \n" +
        "    AND hall_service_best_for LIKE %?3% \n" +
        "    AND NOT EXISTS (SELECT *\n" +
        "                     FROM bfs_hall_services_booked t2 \n" +
        "                     WHERE service_booked_from_date BETWEEN ?4\\:\\:date AND ?5\\:\\:date \n" +
        "                       AND service_booked_to_date BETWEEN ?4\\:\\:date AND ?5\\:\\:date  \n" +
        "                       AND t2.service_booked_uuid\\:\\:uuid = t1.hall_service_uuid)\n" +
        ") x                       \n" +
        "WHERE distance < 50 \n" +
        "ORDER BY distance", nativeQuery = true)
    List<HallServiceInformationTable> findByHallServiceLatitudeAndHallServiceLongitude(Double latitude, Double longitude,
                                   String serviceNeededFor, String fromDate, String toDate);
}
