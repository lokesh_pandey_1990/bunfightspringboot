package com.bunfight.services.bfsservices.hallService;

import com.bunfight.model.bfsservices.hallService.HallServiceBookedModel;
import org.springframework.data.repository.CrudRepository;

/*
Author - Lokesh Pandey
*/

public interface HallServiceBooked extends CrudRepository<HallServiceBookedModel, String> {
}
