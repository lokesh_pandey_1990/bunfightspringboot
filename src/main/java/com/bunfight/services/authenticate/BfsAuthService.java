package com.bunfight.services.authenticate;

import com.bunfight.model.authentication.BfsAuthentication;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

import java.util.UUID;

/*
Author - Lokesh Pandey
*/

public interface BfsAuthService extends CrudRepository<BfsAuthentication, Object> {

    BfsAuthentication findByUserEmailId(String emailId);
    BfsAuthentication findByUserPhoneNumber(Long phoneNumber);

    @Transactional
    @Modifying
    @Query(value = "UPDATE bfs_user_auth_table SET user_email_verified = true WHERE \n" +
            "user_uuid=?1\\:\\:uuid",
        nativeQuery = true)
    void updateEmailVerified(UUID userUuid);
}
